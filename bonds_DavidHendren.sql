--Q1
select * from bond where CUSIP = '28717RH95';

--Q2
select * from bond order by maturity;

--Q3
select CUSIP, quantity*price as 'Portfolio Value' from bond;

--or
select sum(quantity*price) as 'Portfolio Value' from bond;

--Q4
select CUSIP, quantity*(coupon/100) as 'Annual Return' from bond;

--Q5
select bond.CUSIP, bond.quantity, bond.price, bond.coupon, bond.maturity, bond.calldate, bond.rating, rating.ordinal, rating.expected_yield
 from bond join rating on rating.rating = bond.rating where ordinal <='3';

--Q6
select rating, avg(price) as 'price' , avg(coupon/100) as 'coupon' from bond group by rating;

--Q7
--select coupon/price as 'yield' from bond;

select bond.CUSIP, rating.rating from bond join rating on rating.rating = bond.rating where bond.coupon/bond.price < rating.expected_yield;

