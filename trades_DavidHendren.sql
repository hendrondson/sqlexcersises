--Q1 
select * from trade 
join position on position.opening_trade_id = trade.id or position.closing_trade_id = trade.id
where position.trader_id = 1 and trade.stock = 'MRK'
order by trade.size;

--Q2
select sum(trade.size * trade.price)
from trade 
  join position on position.opening_trade_id = trade.id or position.closing_trade_id = trade.id
where position.trader_id = 1 and trade.stock = 'MRK'
